@extends('layout.master')
@section('judul')
<h2><b>Buat Account Baru</b></h2>
@endsection
@section('content')
<form action="/welcome" method="post">
    @csrf
    <b>Sign Up Form</b><br><br>
    <label>First Name :</label><br>
    <input type="text" name="nama"><br><br>
    <label>Last Name :</label><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender</label><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br><br>
    <label>Nationality</label><br>
    <select name="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapura">Singapura</option>
        <option value="Jepang">Jepang</option>
        <option value="Palestina">Palestina</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" value="English">English<br>
    <input type="checkbox" value="Other">Other<br><br>
    <label>Bio</label><br>
    <textarea name="message" rows="10" cols="50"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
            