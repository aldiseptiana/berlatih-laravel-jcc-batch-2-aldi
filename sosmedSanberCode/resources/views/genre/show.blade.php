@extends('layout.master')
@section('judul')
<h1>Halaman Detail Genre</h1>
@endsection

@section('content')

<h1>{{$genre->nama}}</h1>

<div class="row">
@forelse ($genre->film as $item)
    <div class="col-3">
        <div class="card">
            <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" width="100" height="300" alt="...">
            <div class="card-body">
              <h5><b>{{Str::limit($item->judul, 18)}}</b></h5>
              <p class="card-text">{{ Str::limit($item->ringkasan, 100) }} </p>
              <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
              </form>
            </div>
        </div>
    </div>
@empty
    <h1>Tidak ada Film</h1>
@endforelse
</div>

@endsection
