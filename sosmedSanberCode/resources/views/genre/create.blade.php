@extends('layout.master')
@section('judul')
<h1>Halaman Tambah Genre</h1>
@endsection

@section('content')
    <form action="/genre" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
             <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
