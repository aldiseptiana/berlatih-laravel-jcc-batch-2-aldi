@extends('layout.master')
@section('judul')
<h1>Halaman Daftar Genre</h1>
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary my-3">Tambah Genre</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>
                 
                    <form action="/genre/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
              </td>
          </tr>
      @empty
          
      @endforelse
    </tbody>
  </table>
@endsection