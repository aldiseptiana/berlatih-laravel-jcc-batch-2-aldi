@extends('layout.master')
@section('judul')
<h1>Halaman Edit Genre</h1>
@endsection

@section('content')
    <form action="/genre/{{$genre->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" value="{{$genre->nama}}" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
            <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
