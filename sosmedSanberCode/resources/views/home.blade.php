@extends('layout.master')
@section('judul')
<h1>Media Online</h1>
@endsection
@section('content')
<h2><b>Sosial Media Developer</b></h2>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
<h3><b>Benefit Join di Media Online</b></h3>
<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3><b>Cara Bergabung ke Media Online</b></h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection