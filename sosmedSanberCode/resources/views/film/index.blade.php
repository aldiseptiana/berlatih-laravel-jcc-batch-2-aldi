@extends('layout.master')
@section('judul')
<h1>Halaman List Film</h1>
@endsection

@section('content')

<a href="/film/create" class="btn btn-success my-2">Tambah Film</a>

<div class="row">

    @forelse ($film as $item)
        <div class="col-3">
            <div class="card">
                <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" width="100" height="300" alt="...">
                <div class="card-body">
                  <h5><b>{{Str::limit($item->judul, 18)}}</b></h5>
                  <span class="badge badge-secondary">{{$item->genre->nama}}</span>
                  <p class="card-text">{{ Str::limit($item->ringkasan, 100) }} </p>
                  <form action="/film/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                    <input type="submit" class="btn btn-danger btn" value="Delete">
                  </form>
                </div>
              </div>
        </div>
        
    @empty
        <h4>Data Kosong</h4>
    @endforelse
</div>

@endsection