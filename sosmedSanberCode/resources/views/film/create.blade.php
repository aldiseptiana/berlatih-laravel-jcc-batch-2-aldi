@extends('layout.master')
@section('judul')
<h1>Halaman Tambah Film</h1>
@endsection

@section('content')
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Judul</label>
            <input type="text" class="form-control" name="judul">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Ringkasan</label>
            <textarea name="ringkasan" class="form-control"></textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Tahun</label>
            <input type="text" class="form-control" name="tahun">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Poster</label>
            <input type="file" class="form-control" name="poster">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" id="" class="form-control">
                <option value="" disabled>--Pilih Genre--</option>
                @foreach ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    
                @endforeach
            </select>
        </div>
        @error('genre_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
             <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
