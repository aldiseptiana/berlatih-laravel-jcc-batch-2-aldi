@extends('layout.master')
@section('judul')
<h1>Halaman List Film</h1>
@endsection

@section('content')

    <div class="row">
            <div class="col-12">

                    <img src="{{asset('poster/' . $film->poster)}}"  width="200px" alt="...">

                        <h3>{{$film->judul}}</h3>
                        <p class="card-text">{{$film->ringkasan}} </p>
                        <p class="card-text">Rilis Tahun {{$film->tahun}} </p>
                        <p class="card-text">Kategori {{$film->genre->nama}} </p>
                        <a href="/film" class="btn btn-primary">Kembali</a>
             </div>
            </div>
            <h1>List Kritik</h1>
            @forelse ($film->kritik as $item)
            <div class="card">
                <div class="card-header bg-secondary">
                  <b>{{$item->user->name}}</b>
                </div>
                <div class="card-body">
                  <h5 class="card-title">{{$item->point}} Point</h5>
                  <p class="card-text">{{$item->content}}</p>
                </div>
              </div>
                
            @empty
                <h1>Belum Ada Kritik</h1>
            @endforelse

            
            @auth            
            <form action="/kritik" method="POST">
            @csrf
            <input type="hidden" value="{{$film->id}}" name="film_id">
            <div class="form-group">
            <label>Ratting</label>
            <input type="number" class="form-control" name="point">            
            </div>
            @error('point')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
            <label>Kritik</label>
            <textarea name="content" class="form-control"></textarea>
            </div>
            @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <input type="submit" value="tambah">
        </form>
            @endauth



@endsection