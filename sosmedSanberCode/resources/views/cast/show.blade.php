@extends('layout.master')
@section('judul')
<h1>Halaman Detail Cast</h1>
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}} Tahun</p>
<p>{{$cast->bio}}</p>

@endsection
