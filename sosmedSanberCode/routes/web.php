<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//testing
// Route::get('/master', function()
// {
//     return view('layout.master');
// });

Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

Route::get('/table', function(){
    return view('halaman.table');
});

// Route::get('/', 'HomeController@dasboard');
Route::get('/', 'FilmController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@welcome');





//CRUD GAME
//creat data
Route::get('/game/create', 'gameController@create');
//menyimpad data kedalam table game
Route::post('/game', 'gameController@store');

//menampilkan semua data 
Route::get('/game', 'gameController@index');

//menampilkan detail data
Route::get('/game/{game_id}', 'gameController@show');

//form edit data game
Route::get('/game/{game_id}/edit', 'gameController@edit');

//update data game
Route::put('/game/{game_id}', 'gameController@update');

//delete data game
Route::delete('/game/{game_id}', 'gameController@destroy');


Route::group(['middleware' => ['auth']], function () {
    //

    //CRUD CAST
//creat data
Route::get('/cast/create', 'castController@creat');
//menyimpan data ke table cast
Route::post('/cast', 'castController@store');

//menampilkan semua data
Route::get('/cast', 'castController@index');

//detail data
Route::get('/cast/{cast_id}', 'castController@show');

//form edit data cast
Route::get('/cast/{cast_id}/edit', 'castController@edit');

//update data cast
Route::put('/cast/{cast_id}', 'castController@update');

//delete data cast
Route::delete('/cast/{cast_id}', 'castController@destroy');



//CRUD ORM GENRE
Route::resource('genre', 'GenreController');

//Profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'update']);

Route::resource('kritik', 'KritikController')->only([
    'store']);
});
//CRUD ORM FILM
Route::resource('film', 'FilmController');

Auth::routes();


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
