<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class gameController extends Controller
{
    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'required',
            ],
            [
                'name.required' => 'Name Game Harus diisi!!!',
                'gameplay.required'  => 'Gameplay harus di isi!!!',
                'developer.required'  => 'Masukan Nama Developer Terlebih Dahulu!!!',
                'year.required' => 'Masukan Tahun Pembuatan Game!!!',
            ]
        );
    
            DB::table('game')->insert(
                [
                    'name' => $request['name'],
                    'gameplay' => $request['gameplay'],
                    'developer' => $request['developer'],
                    'year' => $request['year']
                ]
            );
    
            return redirect('/game');
    }

    public function index()
    {
        $game = DB::table('game')->get();
 
        return view('game.index', compact('game'));
    }

    public function show($game_id)
    {
        $game = DB::table('game')->where('id', $game_id)->first();

        return view('game.show', compact('game'));
    }

    public function edit($game_id)
    {
        $game = DB::table('game')->where('id', $game_id)->first();

        return view('game.edit', compact('game'));
    }

    public function update($game_id, Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'required',
            ],
            [
                'name.required' => 'Name Game Harus diisi!!!',
                'gameplay.required'  => 'Gameplay harus di isi!!!',
                'developer.required'  => 'Masukan Nama Developer Terlebih Dahulu!!!',
                'year.required' => 'Masukan Tahun Pembuatan Game!!!',
            ]
        );
    
            DB::table('game')->where('id', $game_id)
            ->update(
                [
                    'name' => $request['name'],
                    'gameplay' => $request['gameplay'],
                    'developer' => $request['developer'],
                    'year' => $request['year']
                ]
            );
    
            return redirect('/game');
    }

    public function destroy($game_id)
    {
        DB::table('game')->where('id', '=', $game_id)->delete();

        return redirect('/game');
    }


}
