<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('halaman.pendaftaran');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $nama = $request['nama'];
        $lastname = $request['lastname'];

        return view('halaman.welcome', compact('nama', 'lastname'));
    }
}
