<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class castController extends Controller
{
    public function creat()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
        [
            'nama' => 'required',
            'umur' => 'required|max:2',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama tidak boleh kosong!!!',
            'umur.required'  => 'Masukan Umur mu',
            'bio.required'  => 'Isi Bio Terlebih Dahulu',
        ]
    );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('id', $cast_id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($cast_id, Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|max:2',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!!!',
                'umur.required'  => 'Masukan Umur mu',
                'bio.required'  => 'Isi Bio Terlebih Dahulu',
            ]
        );
        DB::table('cast')->where('id', $cast_id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        DB::table('cast')->where('id', '=', $cast_id)->delete();

        return redirect('/cast');
    }
}
