<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Film;
Use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');
    }

    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required',
                'poster' => 'required|mimes:jpeg,jpg,png|max:2200',
                'genre_id' => 'required',

            ],
            [
                'judul.required' => 'Judul Harus Diisi',
                'ringkasan.required' => 'Masukan Ringkasan Sebagai Spoiler',
                'tahun.required' => 'Masukan Tahun Pembuatan Film',
                'poster.required' => 'Tambahkan Poster Agar Menarik',
                'poster.mimes' => 'Poster hanya berformat jpeg, jpg, dan png Max 2200kb',
                'genre_id.required' => 'Pilih Genre yang Sesuai',
            ]
        );

            $gambar = $request->poster;
            $new_gambar = time() . " - " . $gambar->getClientOriginalName();
    
            $film = new Film;
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->poster = $new_gambar;
            $film->genre_id = $request->genre_id;
            $film->save();

            $gambar->move('poster/', $new_gambar);

            return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);
        $genre = Genre::all();
        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required',
                'poster' => 'mimes:jpeg,jpg,png|max:2200',
                'genre_id' => 'required',

            ],
            [
                'judul.required' => 'Judul Harus Diisi',
                'ringkasan.required' => 'Masukan Ringkasan Sebagai Spoiler',
                'tahun.required' => 'Masukan Tahun Pembuatan Film',
                'poster.mimes' => 'Poster hanya berformat jpeg, jpg, dan png Max 2200kb',
                'genre_id.required' => 'Pilih Genre yang Sesuai',
            ]
           
        );

        $film = Film::find($id);
        if($request->has('poster')) {
            $path = "poster/";
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $new_gambar = time() . " - " . $gambar->getClientOriginalName();
            $gambar->move('poster/', $new_gambar);

            $film->poster = $new_gambar;
        }

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
            $film->save();

            return redirect('/film');


        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $path = "poster/";
            File::delete($path . $film->poster);

        $film->delete();
        return redirect('/film');
    }
}
