<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Genre;


class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genre = DB::table('genre')->get();
 
        return view('genre.index', compact('genre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required'

            ],
            [
                'nama.required' => 'Nama Genre tidak boleh kosong!!!'
            ]
        );
    
            DB::table('genre')->insert(
                [
                    'nama' => $request['nama']
                ]
            );
    
            return redirect('/genre');
        }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($genre_id)
    {
        // $genre = DB::table('genre')->where('id', $genre_id)->first();

        //ORM
        $genre = Genre::findOrFail($genre_id);

        return view('genre.show', compact('genre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($genre_id)
    {
        $genre = DB::table('genre')->where('id', $genre_id)->first();

        return view('genre.edit', compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $genre_id)
    {
        $request->validate(
            [
                'nama' => 'required',
            ],
            [
                'nama.required' => 'Nama Genre tidak boleh kosong!!!',
            ]
        );
        DB::table('genre')->where('id', $genre_id)
            ->update(
                [
                    'nama' => $request['nama'],
                ]
            );
        return redirect('/genre');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($genre_id)
    {
        DB::table('genre')->where('id', '=', $genre_id)->delete();

        return redirect('/genre');
    }
}
